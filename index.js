// let name = 'Budi';

// console.log(name);
// name = 'Haris'

// console.log(name);

// const lastName = 'John'
// console.log(lastName);

// console.log(lastName);

// let age = 10

// let obj = {
//     firstName: 'Haris',
//     lastName: 'Pratama',
//     age: 25,
// }

// console.log(obj);

// let arr = [1, 'Budi', 2, 3, '5', true]

// console.log(arr[1]);
// console.log(arr.length);

// let bool = false | true

// const ifTrue = 3

// if (ifTrue == 1) {
//     console.log('ini true');
// } else if (ifTrue === 2) {
//     console.log('value nya 2');
// } else {
//     console.log('default condition');
// }

// switch (ifTrue) {
//     case 1:
//         console.log('valuenya 1');
//         return 
//     case 2:
//         console.log('valuenya 2');
//         break;
//     default:
//         console.log('Default');
//         break;
// }


// for (let i = 1; i <= 10; i++) {
//     console.log('hallo', i);

//     for (let j = 0; j < 5; j++) {
//         console.log(j, '<< j');
//     }
// }

// let object = {
//     firstName: 'Budi',
//     lastName: 'Anduk',
//     age: 24,
// }

// let arr = [
//     {
//         name: 'John',
//         age: 24
//     },
//     {
//         name: 'Budi',
//         age: 32
//     },
// ]

// const nums = [1,3,4,5,9,1]

// for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i].age);
// }

// for (const key in object) {
//     console.log(key);

//     console.log(object[key], '=== ini value dari property', key);
// }

// function sayHello2(name) {
//     console.log('Hello ' + name);
// }

// const sayHello = (name) => {
//     return 'Hallo ' + name
// }

// sayHello2('Haris')

// const sayHalloToMyFriend = sayHello('John')
// const sayHalloToMyFriend = sayHello

// console.log(sayHalloToMyFriend('John'));

class Car {
    name = 'Avanza'
    detail = null;

    constructor(nameParam, detail) {
        if (nameParam && detail) {
            this.name = nameParam
            this.detail = detail
        }
    }

    getDetail = (price) => {
        return `My car is ${this.name} type ${this.detail.type} km: ${this.detail.km} price: ${price}`
    }

    // getDetail() {

    // }
}


const detailCar = {
    type: 'Honda',
    km: 1000,
}

const newCar = new Car('CRV', detailCar)

// console.log(newCar.name);

// console.log(newCar.getDetail(20000000));

class NewCar extends Car {
    nameFromParent = ''
    detailFromParent = ''

    constructor(name, detail) {
        super(name, detail)
    }

}

const car = new NewCar('XPANDER', {type: 'MITSUBISHI', km: 2000})

console.log(car.getDetail(120000000));

car.name = 'Veloz'

console.log(new Car())
console.log(car);

const arr = [
    {
        id:1,
        name: 'Haris',
        email: 'h@mail.com'
    },
    {
        id:2,
        name: 'Haris',
        email: 'h@mail.com'
    },
    {
        id:3,
        name: 'Haris',
        email: 'h@mail.com'
    },
]

// arr.shift()
// arr.unshift(4)

let output = []

// for (let i = 0; i < arr.length; i++) {
//     output.push({
//         ...arr[i],
//         role: 'member'
//     })
// }

const newArr = arr.map((item, idx) => {
    return {
        ...item,
        role: 'member',
        userKe: idx+1
    }
})

console.log(newArr);

